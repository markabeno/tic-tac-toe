# README #

This is a simple tic tac toe game written in python.

Game Rules

1. Players can select the complexity of the board grid system. 3x3, 6x6 etc.

2. Player 1’s marker is ‘X’, Player 2’s marker is ‘O’.

3. The two players take turns placing their marker on the board, starting with Player 1.

4. The players can place their marker on any vacant spot.

5. The objective of the game is for a player to get three of their markers in a row

(horizontally, vertically or diagonally).



Enjoy! :)