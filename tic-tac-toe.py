try:
    grid = raw_input('Please enter board grid. If you want to play 3x3, Enter 3: ')
    grid = int(grid)
except:
    print('Invalid grid format')
    exit()

p1 = raw_input('Enter name for Player 1: ')
p2 = raw_input('Enter name for Player 2: ')

noWinner = True
p1ans = []
p2ans = []
pTurn = 1
w = int(grid)
h = int(grid)

def build_board(entry=''):
    """
    Draw a grid given the width and height
    Returns true if a winning combination is detected.
    :param entry:
    :return:
    """
    b = []
    c = 1
    global w, h
    for row in range(w):
        b.append([])
        for col in range(h):
            global p1ans, p2ans
            if c in p1ans:
                b[row].append('X')
            elif c in p2ans:
                b[row].append('O')
            else:
                b[row].append(str(c))
            c += 1

    for row in b:
        col = [col for col in row]
        print(" | ".join(col))
        print("----" * len(col))

    if diagonal_checker(b, entry):
        return True


def diagonal_checker(b, entry):
    """
    Checks for all possible combination format (Horizontal, Vertical and Diagonal)
    :param b:
    :param entry:
    :return:
    """
    dlr = []
    drl = []
    i = 0
    global w, h

    for row in b:

        # Get the diagonal match from left to right
        dlr.append(row[i % w])
        # Get the diagonal match from right to left
        drl.append(row[-1 - i])

        i += 1

        if row.count(entry) == w:
            # print('horizontal match')
            return True
        elif dlr.count(entry) == w:
            # print('diagonal LR match')
            return True
        elif drl.count(entry) == w:
            # print('diagonal RL match')
            return True

    # Get the vertical match combinations using zip
    for row in zip(*b):
        if row.count(entry) == w:
            # print('vertical match')
            return True

    return False

# Intialize the board
build_board()

while noWinner:

    if pTurn == 1:
        try:
            ans = raw_input('{0}, choose a box to place an x into: '.format(p1))
            p1ans.append(int(ans))
            if build_board('X'):
                print('Congratulations {0}! You have won.'.format(p1))
                break
            pTurn = 0
        except Exception as e:
            print('Invalid format. Please enter the number inside the box')
            pTurn = 1
            continue
    else:
        try:
            ans = raw_input('{0}, choose a box to place an o into: '.format(p2))
            p2ans.append(int(ans))
            if build_board('O'):
                print('Congratulations {0}! You have won.'.format(p2))
                break
            pTurn = 1
        except Exception as e:
            print('Invalid format. Please enter the number inside the box')
            pTurn = 0
            continue

    # Check if slots has been taken.
    if (len(p1ans) + len(p2ans)) == w*h:
        print('DRAW!')
        break